﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Fornecedores;
using Modelos.Funcionarios;
using Modelos.Excessoes;
using System.Text.RegularExpressions;

namespace Modelos.Mercadorias
{
    class Mercadoria
    {
        private int EstoqueTotal { get; set; }
        public int CodigoBarras { get;}
        public string Nome { get; }
        public int Quantidade { get; set; }
        public double PrecoUnidadeFornecedor { get; }
        public double PrecoUnidadeCliente { get; set; }
        public Fornecedor Fornecedor { get;}
        public Funcionario Funcionario { get; }

        public Mercadoria(int codigoBarras, string nome, int quantidade, double precoUnidadeFornecedor, Fornecedor fornecedor, Funcionario funcionario, string senhaFuncionario)
        {
            if (!funcionario.Autenticar(senhaFuncionario))
            {
                throw new SenhaInvalidaException("A senha incorreta.");
            }

            if (senhaFuncionario.Contains("Ger") == false && senhaFuncionario.Contains("Mitrava") == false)
            {
                throw new PermissaoNegadaException($"O funcionario {funcionario.NomeCompleto} não tem permissão para cadastrar mercadorias.");
            }

            CodigoBarras = codigoBarras;
            Nome = nome;
            Quantidade = quantidade;
            PrecoUnidadeFornecedor = precoUnidadeFornecedor;
            PrecoUnidadeCliente = 2 * PrecoUnidadeFornecedor;
            Fornecedor = fornecedor;
            EstoqueTotal += quantidade;
            Funcionario = funcionario;
        }

        public void ModificarPreco(double valorModificado)
        {
            PrecoUnidadeCliente = valorModificado;
            Console.WriteLine($"O preço da mercadoria foi modificado para {PrecoUnidadeCliente}");
        }


    }
}
