﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Fornecedores;
using Modelos.Funcionarios;

namespace Modelos.Mercadorias
{
    class Camisas : Mercadoria
    {
        public string Tamanho { get; }
        public Camisas(int codigoBarras, 
            string nome, 
            int quantidade,
            double precoUnidade,
            Fornecedor fornecedor,
            Funcionario funcionario,
            string senha,
            string tamanho) : 
            base(codigoBarras, 
                nome, 
                quantidade,
                precoUnidade,
                fornecedor,
                funcionario,
                senha)
        {
            Tamanho = tamanho;
            Console.WriteLine($"Mercadoria {nome} acrescentada com sucesso ao estoque");
        }

    }
}
