﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Fornecedores;
using Modelos.Funcionarios;
    

namespace Modelos.Mercadorias
{
    class Calcas:Mercadoria
    {
        public int Tamanho { get; }
        public Calcas(int codigoBarras,
            string nome,
            int quantidade,
            double precoUnidade,
            Fornecedor fornecedor,
            Funcionario funcionario,
            string senha,
            int tamanho) :
            base(codigoBarras,
                nome,
                quantidade,
                precoUnidade,
                fornecedor,
                funcionario,
                senha)
        {
            Tamanho = tamanho;
            Console.WriteLine($"Mercadoria {nome} acrescentada com sucesso ao estoque");
        }
    }
}
