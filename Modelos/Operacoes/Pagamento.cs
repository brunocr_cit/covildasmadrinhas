﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Excessoes;
using Modelos.Clientes;


namespace Modelos.Operacoes
{
    class Pagamento
    {
        //Cartão de crédito/débito, em dinheiro, parcelado no crediário
        public string FormaPagamento { get; } 
        public int QuantidadeParcelas { get; }
        public double ValorParcelas { get; }

        public Pagamento(string formaPagamento, int quantidadeParcelas, double valorVenda, Cliente cliente)
        {
            if (cliente == null && formaPagamento == "parcelado no crediário")
            {
                throw new FormaPagamentoInvalidoException("Um cliente não cadastrado não pode realizar o pagamento por meio de parcelas no crediário.");
            }

            FormaPagamento = formaPagamento;

            if (quantidadeParcelas < 1 || quantidadeParcelas > 10)
            {
                throw new ArgumentException("A quantidade de parcelas não pode ser menor que 1 e nem maior que 10.");
            }

            QuantidadeParcelas = quantidadeParcelas;
            ValorParcelas = valorVenda / quantidadeParcelas;

            CaixaRegistradora.EntradaDinheiro(ValorParcelas); 

        }
       
    }
}
