﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Mercadorias;
using Modelos.Funcionarios;
using Modelos.Excessoes;
using Modelos.Clientes;

namespace Modelos.Operacoes
{
    class DevolucaoSimples
    {
        public static int QuantidadeDevolucao { get; private set; }
        public int CodigoDevolucao { get; }
        public Mercadoria Produto { get; }
        public Funcionario Vendedor { get; }
        public Cliente AqueleQueDevolve { get; }
        public VendaSimples VendaRelacionada { get; }
        public int QuantidadeMercadoria { get; }
        public double ValorDevolucao { get; }

        public DevolucaoSimples(Mercadoria produto,
            Funcionario vendedor,
            Cliente cliente,
            VendaSimples vendaRelacionada,
            int quantidadeMercadoria)
        {
            Produto = produto;
            Vendedor = vendedor;
            AqueleQueDevolve = cliente;
            VendaRelacionada = vendaRelacionada;
            QuantidadeMercadoria = quantidadeMercadoria;

            if (QuantidadeMercadoria > VendaRelacionada.QuantidadeMercadoria)
            {
                throw new QuantidadeExcedenteException($"Tentativa de devolver {QuantidadeMercadoria} unidades, porém foram levadas {VendaRelacionada.QuantidadeMercadoria} unidades na compra inicial.");
            }

            ValorDevolucao = QuantidadeMercadoria * Produto.PrecoUnidadeCliente;
            CaixaRegistradora.SaidaDinheiro(-ValorDevolucao);
            Produto.Quantidade += QuantidadeMercadoria;
            QuantidadeDevolucao++;
            CodigoDevolucao = QuantidadeDevolucao;
        }

        public void DetalhesDevolucao()
        {
            Console.WriteLine($"O cliente {this.AqueleQueDevolve.NomeCompleto} está devolvendo " +
                        $"{this.QuantidadeMercadoria} peças do produto {this.Produto.Nome}");
            Console.WriteLine($"Restam no estoque {this.Produto.Quantidade} unidades.");
            Console.WriteLine($"Dinheiro em caixa R$ {CaixaRegistradora.QuantidadeDinheiro}");
            Console.WriteLine($"Até o momento já foram executadas {DevolucaoSimples.QuantidadeDevolucao} devoluções");
        }
    }
}
