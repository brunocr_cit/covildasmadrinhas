﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Operacoes
{
   class CaixaRegistradora
    {
        public static double QuantidadeDinheiro { get; private set; }

        public static void EntradaDinheiro(double valorEntrada)
        {
            if (valorEntrada <0)
            {
                throw new ArgumentException("O valor de dinheiro acrescido ao caixa não pode ser negativo.");
            }
            QuantidadeDinheiro += valorEntrada;
        }

        public static void SaidaDinheiro(double valorSaida)
        {
            if (valorSaida > 0)
            {
                throw new ArgumentException("O valor de dinheiro decrescido do caixa não pode ser positivo.");
            }
            QuantidadeDinheiro += valorSaida;
        }
    }
}
