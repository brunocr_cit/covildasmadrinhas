﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Mercadorias;
using Modelos.Funcionarios;
using Modelos.Excessoes;
using Modelos.Clientes;

namespace Modelos.Operacoes
{
    class VendaSimples
    {
        public static int QuantidadeVendas { get; private set; }
        public int CodigoVenda { get;}
        public Mercadoria Produto { get;}
        public Vendedor Vendedor { get;}
        public Cliente Comprador { get; }
        public Pagamento Pagar { get; set; }
        public int QuantidadeMercadoria { get;}
        public double ValorVenda { get;}

        public VendaSimples(Mercadoria produto, 
            Vendedor vendedor, 
            Cliente cliente, 
            int quantidadeMercadoria, 
            string formaPagamento , 
            int quantidadeParcelas )
        {
            Produto = produto;
            Vendedor = vendedor;
            Comprador = cliente;
            QuantidadeMercadoria = quantidadeMercadoria;

            if (QuantidadeMercadoria > Produto.Quantidade)
            {
                throw new QuantidadeExcedenteException($"Tentativa de comprar {QuantidadeMercadoria} unidades da mercadoria {Produto.Nome}, porém só temos {Produto.Quantidade} unidades no estoque.");
            }

            ValorVenda = QuantidadeMercadoria * Produto.PrecoUnidadeCliente;
            Vendedor.AcrescentarComissaoVendaNoSalario(ValorVenda);
            Produto.Quantidade -= QuantidadeMercadoria;
            Pagar = new Pagamento(formaPagamento, quantidadeParcelas, ValorVenda, Comprador);
            QuantidadeVendas++;
            CodigoVenda = QuantidadeVendas;
        }
         
        public void DetalhesVenda()
        {
            Console.WriteLine($"O cliente {this.Comprador.NomeCompleto} está fazendo uma compra de {this.QuantidadeMercadoria} " +
                $"unidades da mercadoria {this.Produto.Nome} totalizando de R$ {this.ValorVenda}, " +
                $"com o método de pagamento {this.Pagar.FormaPagamento.ToLower()}, sendo parcelado em {this.Pagar.QuantidadeParcelas} " +
                $"vez(es) com a funcionária {this.Vendedor.NomeCompleto}.");
            Console.WriteLine(this.Vendedor.ToString());
            Console.WriteLine($"Restam no estoque {this.Produto.Quantidade} unidades.");
            Console.WriteLine($"Dinheiro em caixa R$ {CaixaRegistradora.QuantidadeDinheiro}");
            Console.WriteLine($"Até o momento já foram executadas {VendaSimples.QuantidadeVendas} vendas");

        }

    }
}
