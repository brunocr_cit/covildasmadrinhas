﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Interfaces
{
    public interface I_Autenticavel
    {
        bool Autenticar(string senha);
    }
}
