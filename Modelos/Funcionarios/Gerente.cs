﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Funcionarios
{
    class Gerente : Funcionario
    {
        public Gerente(int codigoFuncionario, string nomeCompleto, string nomeDaMae,
               string cpf, string telefoneContato, string rua, string numero, string bairro,
               string cep, string cidade, string estado, string pis, double salario, string senha)
               : base(codigoFuncionario, nomeCompleto, nomeDaMae, cpf, telefoneContato,
                   rua, numero, bairro, cep, cidade, estado, pis, salario, senha)
        {
            Console.WriteLine($"Novo(a) gerente criado com sucesso, bem vindo(a) {nomeCompleto}");
        }

    }
}
