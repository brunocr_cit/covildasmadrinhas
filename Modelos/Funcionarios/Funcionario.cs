﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos.Interfaces;
using Modelos.Auxiliares;

namespace Modelos.Funcionarios
{
    class Funcionario : Pessoa,I_Autenticavel
    {
        private Aux_Autenticacao aux_Autenticacao = new Aux_Autenticacao();
        public int CodigoFuncionario { get;}
        public string PIS { get;}
        public double Salario { get; private set; } 
        public string Senha { get; set; }
        public Funcionario(int codigoFuncionario, string nomeCompleto, string nomeDaMae,
            string cpf, string telefoneContato, string rua, string numero, string bairro,
            string cep, string cidade, string estado, string pis, double salario, string senha)
            : base(nomeCompleto, nomeDaMae, cpf, telefoneContato, rua, numero,
                bairro, cep,  cidade, estado)
        {
            CodigoFuncionario = codigoFuncionario;
            PIS = pis;
            Salario = salario;
            Senha = senha;

        }
        public bool Autenticar(string senha)
        {
            return aux_Autenticacao.CompararSenhas(Senha, senha);
        }
        public void ModificarSalario(double novoSalarioBase)
        {
            if (novoSalarioBase < 0)
            {
                throw new ArgumentException($"O novo salário não pode ser menor que zero, como o apresentado {novoSalarioBase}");
            }

            Salario = novoSalarioBase;
            Console.WriteLine($"O salário base foi atualizado para {Salario} com sucesso.");
        }
    }
}
