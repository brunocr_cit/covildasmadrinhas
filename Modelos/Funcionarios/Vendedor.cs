﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Funcionarios
{
    class Vendedor : Funcionario
    {
        public double SalarioTotal { get; private set; }
        public double ComissaoVenda { get; private set; }
        public Vendedor(int codigoFuncionario, string nomeCompleto, string nomeDaMae,
            string cpf, string telefoneContato, string rua, string numero, string bairro,
            string cep, string cidade, string estado, string pis, double salario, string senha) 
            : base(codigoFuncionario, nomeCompleto, nomeDaMae, cpf, telefoneContato,
                rua, numero, bairro, cep,  cidade, estado, pis, salario, senha)
        {
            SalarioTotal = salario;
            Console.WriteLine($"Novo(a) vendedor(a) criado(a) com sucesso, bem vindo(a) {nomeCompleto}");
        }

        public double AcrescentarComissaoVendaNoSalario(double valorVenda)
        {
            if (valorVenda < 0)
            {
                throw new ArgumentException("O valor da venda não pode ser negativo.");
            }
            ComissaoVenda = 0.1 * valorVenda;
            SalarioTotal += ComissaoVenda;
            return ComissaoVenda;
        }

        public override string ToString()
        {
            return $"O salário base da vendedora {NomeCompleto} é {Salario}, que recebe um acrescimo da comissão igual R$ {ComissaoVenda}, cujo total atual é R${SalarioTotal}";
        }

    }
}
