﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Clientes
{
    class Cliente : Pessoa
    {
        public int PontosBomPagador { get; set; }
        public Cliente(string nomeCompleto,
            string nomeDaMae,
            string cpf,
            string telefoneContato,
            string rua,
            string numero,
            string bairro,
            string cep,
            string cidade,
            string estado) : base(
                nomeCompleto, 
                nomeDaMae, 
                cpf, 
                telefoneContato,
                rua,
                numero,
                bairro,
                cep,
                cidade,
                estado)
        {
            Console.WriteLine($"Novo(a) cliente criado(a) com sucesso, bem vindo(a) {nomeCompleto}");

        }
    }
}
