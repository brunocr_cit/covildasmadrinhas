﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Fornecedores
{
    class Fornecedor
    {
        public int CodigoFornecedor { get; set; }
        public string NomeEmpresa { get; set; }
        public string CNPJ { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public Endereco Endereco { get; set; }

        public Fornecedor(int codigoFornecedor,
            string nomeEmpresa,
            string cnpj,
            string emailContato,
            string telefoneContato,
            string rua,
            string numero,
            string bairro,
            string cep,
            string cidade,
            string estado)
        {
            CodigoFornecedor = codigoFornecedor;
            NomeEmpresa = nomeEmpresa;
            CNPJ = cnpj;
            EmailContato = emailContato;
            TelefoneContato = telefoneContato;
            Endereco = new Endereco(rua, numero, bairro, cep,  cidade, estado);
            Console.WriteLine($"Novo(a) fornecedor(a) criado(a) com sucesso, que nossa parceria seja frutífera {nomeEmpresa}");
        }


    }
}
