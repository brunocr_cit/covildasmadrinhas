﻿using System;
using Modelos.Funcionarios;
using Modelos.Clientes;
using Modelos.Fornecedores;
using Modelos.Mercadorias;
using Modelos.Operacoes;

namespace Modelos
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Gerente SamiraClose = new Gerente(1, "Samira Close",
                       "Mãe Close", "123456789-01", "(01) 9 9876-5432", "Rua Close", "001A",
                       "Bairro Close", "12345-001", "Cidade Close", "Estado Close", "123.45678.90-1", 5000, "Ger_Close");

                Gerente Rebecca = new Gerente(2, "Rebecca",
                    "Mãe Becca", "123456789-02", "(02) 9 9876-5432", "Rua Becca", "002A",
                    "Bairro Becca", "12345-002", "Cidade Becca", "Estado Becca", "123.45678.90-2", 5000, "Ger_Becca");

                Console.WriteLine("");

                Vendedor NickyMitrava = new Vendedor(3, "Nicky Mitrava",
                    "Mãe Mitrava", "123456789-03", "(03) 9 9876-5432", "Rua Mitrava", "003A",
                    "Bairro Mitrava", "12345-003", "Cidade Mitrava", "Estado Mitrava", "123.45678.90-3", 3000, "Ven_Mitrava");

                Vendedor Demuxa = new Vendedor(4, "Demuxa",
                    "Mãe Demi", "123456789-04", "(04) 9 9876-5432", "Rua Demi", "004A",
                    "Bairro Demi", "12345-004", "Cidade Demi", "Estado Demi", "123.45678.90-4", 3000, "Ven_Demi");

                Vendedor Sabrinoca = new Vendedor(5, "Sabrinoca",
                    "Mãe Sabrinaa", "123456789-05", "(05) 9 9876-5432", "Rua Sabrinaa", "005A",
                    "Bairro Sabrinaa", "12345-005", "Cidade Sabrinaa", "Estado Sabrinaa", "123.45678.90-5", 3000, "Ven_Brina");

                Console.WriteLine("");

                Cliente DaniLiu = new Cliente("Dani Liu",
                    "Mãe Liu", "123456789-06", "(06) 9 9876-5432", "Rua Liu", "006A",
                    "Bairro Liu", "12345-006", "Cidade Liu", "Estado Liu");

                Cliente BeehNeto = new Cliente("Beeh Neto",
                    "Mãe Neto", "123456789-07", "(07) 9 9876-5432", "Rua Neto", "007A",
                    "Bairro Neto", "12345-007", "Cidade Neto", "Estado Neto");

                Cliente LuCroft = new Cliente("Lu Croft",
                    "Mãe Croft", "123456789-08", "(08) 9 9876-5432", "Rua Croft", "008A",
                    "Bairro Croft", "12345-008", "Cidade Croft", "Estado Croft");

                Console.WriteLine("");

                Fornecedor WanessWolf = new Fornecedor(1, "Empresa Wolf", "12.345.678/1234-01", "empresawolf@contato.com", "(09) 9 9876-5432", "Rua Wolf", "009A",
                    "Bairro Wolf", "12345-009", "Cidade Wolf", "Estado Wolf");

                Fornecedor MandyMess = new Fornecedor(1, "Empresa Mess", "12.345.678/1234-02", "empresamess@contato.com", "(10) 9 9876-5432", "Rua Mess", "010A",
                    "Bairro Mess", "12345-010", "Cidade Mess", "Estado Mess");

                Console.WriteLine("");

                //Comprando/cadastrando as mercadorias
                Camisas RegatasLukeLeoP = new Camisas(100001, "Regatas masculinas de malha estampadas P", 10, 25, WanessWolf,SamiraClose, "Ger_Close", "P");

                //Erro de senha
                //Camisas RegatasLukeLeoM = new Camisas(100002, "Regatas masculinas de malha estampadas M", 10, 27.50, WanessWolf, Rebecca, "Ger_Rebecca", "M");

                //Erro de permissão;
                //Camisas RegatasLukeLeoG = new Camisas(100003, "Regatas masculinas de malha estampadas G", 10, 27.50, WanessWolf, Demuxa, "Ven_Demi", "G");

                Camisas RegatasLukeLeoGG = new Camisas(100004, "Regatas masculinas de malha estampadas GG", 10, 27.50, WanessWolf, Rebecca, "Ger_Becca", "GG");

                Console.WriteLine("");

                Calcas CalcasJeansMatey38 = new Calcas(100005, "Calças jeans com detalhes em rasgado 38", 10, 42.50, MandyMess, NickyMitrava, "Ven_Mitrava", 38);

                Calcas CalcasJeansMatey40 = new Calcas(100006, "Calças jeans com detalhes em rasgado 40", 10, 45, MandyMess, NickyMitrava, "Ven_Mitrava", 40);

                Console.WriteLine("");

                VendaSimples Venda1 = new VendaSimples(RegatasLukeLeoP, NickyMitrava, BeehNeto, 7, "Á vista", 1);
                Venda1.DetalhesVenda();

                Console.WriteLine("");

                //Erro de venda de mais unidades do que estão disponíveis no estoque
                //VendaSimples Venda2 = new VendaSimples(RegatasLukeLeoP, Demuxa, DaniLiu, 7, "no cartão de crédito", 5);

                //Erro de venda para cliente não cadastrado e método de pagamento sendo parcelado no crediário
                //VendaSimples Venda3 = new VendaSimples(CalcasJeansMatey40, Sabrinoca, null, 5, "parcelado no crediário", 5);

                //Erro de número de parcelas excedendo o limite máximo de 10
                //VendaSimples Venda4 = new VendaSimples(CalcasJeansMatey40, Sabrinoca, LuCroft, 5, "parcelado no crediário", 11);

                VendaSimples Venda5 = new VendaSimples(RegatasLukeLeoGG, NickyMitrava, BeehNeto, 3, "No cartão de crédito", 3);
                Venda5.DetalhesVenda();

                Console.WriteLine("");

                //Erro de devolução em excesso
                //DevolucaoSimples Devolucao1 = new DevolucaoSimples(RegatasLukeLeoP, NickyMitrava, BeehNeto, Venda1, 8);

                //Olhar amarração
                DevolucaoSimples Devolucao2 = new DevolucaoSimples(RegatasLukeLeoP, NickyMitrava, BeehNeto, Venda1, 3);
                Devolucao2.DetalhesDevolucao();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
