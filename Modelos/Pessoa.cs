﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    abstract class Pessoa
    {
        public string NomeCompleto { get; private set; }
        public string NomeDaMae { get; private set; }
        public string CPF { get; private set; }
        public string TelefoneContato { get; set; }
        public Endereco Endereco { get; set; }

        public Pessoa(string nomeCompleto,
            string nomeDaMae,
            string cpf,
            string telefoneContato,
            string rua,
            string numero,
            string bairro,
            string cep,
            string cidade,
            string estado)
        {
            NomeCompleto = nomeCompleto;
            NomeDaMae = nomeDaMae;
            CPF = cpf;
            TelefoneContato = telefoneContato;
            Endereco = new Endereco(rua, numero, bairro, cep, cidade, estado);
        }
    }
}
