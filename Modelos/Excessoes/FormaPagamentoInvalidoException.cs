﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Excessoes
{
    class FormaPagamentoInvalidoException : Exception
    {
        public FormaPagamentoInvalidoException()
        {

        }
        public FormaPagamentoInvalidoException(string mensagem)
            : base(mensagem)
        {
        }

        public FormaPagamentoInvalidoException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
