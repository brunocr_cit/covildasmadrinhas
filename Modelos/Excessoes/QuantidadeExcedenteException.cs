﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Excessoes
{
    class QuantidadeExcedenteException : Exception
    {
        public int QuantidadeSendoDevolvida { get; }
        public int QuantidadeCompradaInicialmente { get; }

        public QuantidadeExcedenteException()
        {

        }

        public QuantidadeExcedenteException(int quantidadeSendoDevolvida, int quantidadeCompradaInicialmente)
        {
            QuantidadeSendoDevolvida = quantidadeSendoDevolvida;
            QuantidadeCompradaInicialmente = quantidadeCompradaInicialmente;
        }

        public QuantidadeExcedenteException(string mensagem)
            : base(mensagem)
        {
        }

        public QuantidadeExcedenteException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
