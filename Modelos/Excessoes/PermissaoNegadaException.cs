﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Excessoes
{
    class PermissaoNegadaException : Exception
    {
        public PermissaoNegadaException()
        {

        }
        public PermissaoNegadaException(string mensagem)
            : base(mensagem)
        {
        }

        public PermissaoNegadaException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
