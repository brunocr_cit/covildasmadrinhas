﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Excessoes
{
    class SenhaInvalidaException : Exception
    {
        public SenhaInvalidaException()
        {

        }
        public SenhaInvalidaException(string mensagem)
            : base(mensagem)
        {
        }

        public SenhaInvalidaException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
